import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function CommonPhrases() {
  // Retrieve the language selected at the home page from localStorage
  const [langaugeSelected] = useState(localStorage.getItem('languageSelected'));
  // Empty variables for all pre-filled areas of the COVID-19 Question 2 page that are changed according to the non-English language selected
  const [componentTitle, setComponentTitle] = useState("title");
  const [phraseAnnual, setPhraseAnnual] = useState("");
  const [phraseMonthly, setPhraseMonthly] = useState("");
  const [phraseFortnightly, setPhraseFortnightly] = useState("");
  const [phraseWeekly, setPhraseWeekly] = useState("");
  const [phraseGrossIncome, setPhraseGrossIncome] = useState("");
  const [phraseNetIncome, setPhraseNetIncome] = useState("");
  const [phraseTaxableIncome, setPhraseTaxableIncome] = useState("");
  const [phraseAgePension, setPhraseAgePension] = useState("");
  const [phraseDisabilityPension, setPhraseDisabilityPension] = useState("");
  const [phraseParentingPayment, setPhraseParentingPayment] = useState("");
  const [phraseWidowsPension, setPhraseWidowsPension] = useState("");
  const [phraseSurvivorsPension, setPhraseSurvivorsPension] = useState("");

  // Function that runs when the page is loaded
  useEffect(() => {
    // Switch statement to assign the pre-filled text with the relevant language chosen at the beginning 
    switch (langaugeSelected) {
      // Vietnamese variables 
      case "vi":
        {
          setComponentTitle("Bản dịch thông thường");
          setPhraseAnnual("Hàng Năm");
          setPhraseMonthly("Hàng tháng");
          setPhraseFortnightly("Mỗi Hai Tuần");
          setPhraseWeekly("​​Hàng Tuần");
          setPhraseGrossIncome("Lợi Tức Chưa Trừ Thuế");
          setPhraseNetIncome("​Lợi Tức Trừ Thuế Rồi");
          setPhraseTaxableIncome("​Số Tiền Chịu Thuế");
          setPhraseAgePension("​Hưu Bổng");
          setPhraseDisabilityPension("Cấp Dưỡng Khiếm Tật");
          setPhraseParentingPayment("Thanh toán nuôi dạy con cái");
          setPhraseWidowsPension("Cấp Dưỡng Quả Phụ");
          setPhraseSurvivorsPension("Cấp Dưỡng Người Sống Sót");
          break;
        }
      // German variables
      case "de":
        {
          setComponentTitle("Gemeinsame Übersetzungen");
          setPhraseAnnual("​jährlich / pro Jahr / im Jahr");
          setPhraseMonthly("​monatlich / pro Monat / im Monat");
          setPhraseFortnightly("​vierzehntägig / zweiwöchentlich / alle zwei Wochen");
          setPhraseWeekly("wöchentlich / pro Woche");
          setPhraseGrossIncome("​Bruttoeinkommen / Bruttoverdienst");
          setPhraseNetIncome("​Nettoeinkommen / Nettoverdienst");
          setPhraseTaxableIncome("steuerpflichtiger Betrag / zu versteuernder Betrag");
          setPhraseAgePension("Altersrente");
          setPhraseDisabilityPension("Erwerbsunfähigkeitsrente");
          setPhraseParentingPayment("Elternzahlung");
          setPhraseWidowsPension("​Witwenrente");
          setPhraseSurvivorsPension("​Hinterbliebenenrente");
          break;
        }
      default:
        console.log("Error: No language selected");
    }
  }, [langaugeSelected]);

  return (
    <div className='commonPhrasesContainer'>
      <div>
        <h2><i>{componentTitle}</i> / <b>Common translations:</b></h2>
        <ul className='commonPhrases'>
          <li>
            <Link to='/translate?custom=Annual'>
              <i>{phraseAnnual}</i><br /><b>Annual</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Monthly'>
              <i>{phraseMonthly}</i><br /><b>Monthly</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Fortnightly'>
              <i>{phraseFortnightly}</i><br /><b>Fortnightly</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Weekly'>
              <i>{phraseWeekly}</i><br /><b>Weekly</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Gross%20Income'>
              <i>{phraseGrossIncome}</i><br /><b>Gross Income</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Net%20Income'>
              <i>{phraseNetIncome}</i><br /><b>Net Income</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Taxable%20Income'>
              <i>{phraseTaxableIncome}</i><br /><b>Taxable Income</b>
            </Link>
          </li >
          <li>
            <Link to='/translate?custom=Age%20Pension'>
              <i>{phraseAgePension}</i><br /><b>Age Pension</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Disability%20Pension'>
              <i>{phraseDisabilityPension}</i><br /><b>Disability Pension</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Parenting%20Payment'>
              <i>{phraseParentingPayment}</i><br /><b>Parenting Payment</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Widows%20Pension'>
              <i>{phraseWidowsPension}</i><br /><b>Widows Pension</b>
            </Link>
          </li>
          <li>
            <Link to='/translate?custom=Survivors%20Pension'>
              <i>{phraseSurvivorsPension}</i><br /><b>Survivors Pension</b>
            </Link>
          </li>
        </ul >
      </div >
    </div >
  );
}

export default CommonPhrases;