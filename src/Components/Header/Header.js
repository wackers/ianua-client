import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import CentrelinkLogo from '../../Images/CentrelinkLogo.JPG';

function Header() {
  // Object to hold history and allow redirecting to other pages
  const history = useHistory();
  const location = useLocation();

  function goHome() {
    // Check if the current page is the landing page or language selection page
    if (location.pathname === "/" || location.pathname === "/selectLanguage") {
      // If true then pressing the header image will direct to the landing page
      console.log("pushing to landing page");
      history.push("/")
    } else {
      // If false then pressing the header image will direct to the language selection page
      console.log("pushing to home page");
      history.push("/selectLanguage");
    }
  }

  return (
    <header>
      <div className="logo">
        <img className="imageBtn" onClick={goHome} src={CentrelinkLogo} alt='Home' />

      </div>
    </header>
  );
}

export default Header;