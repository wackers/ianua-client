import React, { useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
import axios from "axios";
import CommonPhrases from "../CommonPhrases";
import IdleTimeout from "../IdleTimeout";
import SwitchIcon from "../../Images/SwitchIcon.png";
import MicrophoneIcon from "../../Images/MicrophoneIcon.png";
import VolumeIcon from "../../Images/VolumeIcon.png";
import ClearIcon from "../../Images/ClearIcon.png";
import { Spinner } from "reactstrap";
import { Helmet } from "react-helmet";
import * as QueryString from "query-string";

function Translate(props) {
  const [inputTextAreaValue, setInputTextAreaValue] = useState("");
  const [outputTextAreaValue, setOutputTextAreaValue] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  // Variable that determines if English will be displayed as the input language or the output language
  const [isEnglishInput, setIsEnglishInput] = useState(true);
  // Retrieve the language selected at the home page from localStorage
  var [langaugeSelected] = useState(localStorage.getItem('languageSelected'));
  // Empty variables for all pre-filled areas of the translation page that are changed according to the non-English language selected
  const [nonEngTextAreaTitlePt1, setNonEngTextAreaTitlePt1] = useState("");
  const [nonEngTextAreaTitlePt2, setNonEngTextAreaTitlePt2] = useState("");
  const [engTextAreaTitle, setEngTextAreaTitle] = useState("");
  const [textAreaInputPlaceholder, setTextAreaInputPlaceholder] = useState("");
  const [textAreaOutputPlaceholder, setTextAreaOutputPlaceholder] = useState("");
  const [translateButtonText, setTranslateButtonText] = useState("");

  // Function that runs when the page is loaded
  useEffect(() => {
    // Switch statement to assign the pre-filled text with the relevant language chosen at the beginning
    switch (langaugeSelected) {
      // Vietnamese variables 
      case "vi":
        {
          console.log("Language chosen:", "VIETNAMESE");
          setNonEngTextAreaTitlePt1("Tiếng Việt");
          setNonEngTextAreaTitlePt2("Vietnamese");
          setEngTextAreaTitle("Tiếng Anh");
          setTextAreaInputPlaceholder("Nhập văn bản ở đây... / Enter text here...");
          setTextAreaOutputPlaceholder("Dịch... / Translation...");
          setTranslateButtonText("Phiên dịch");
          break;
        }
      // German variables
      case "de":
        {
          console.log("Language chosen:", "GERMAN");
          setNonEngTextAreaTitlePt1("Deutsche");
          setNonEngTextAreaTitlePt2("German");
          setEngTextAreaTitle("Englisch");
          setTextAreaInputPlaceholder("Hier Text eingeben... / Enter text here...");
          setTextAreaOutputPlaceholder("Übersetzung... / Translation...");
          setTranslateButtonText("Übersetzen");
          break;
        }
      default:
        console.log("Error: No language selected");
    }
  }, [langaugeSelected]);

  // Function used when Centrelink jargon phrase is selected from the CommonPhrases component 
  useEffect(() => {
    // Parse the query obtained from a selected jargon phrase
    const params = QueryString.parse(props.location.search);
    const customKey = params.custom;
    // Import the definitions JSON file
    let jargonDefinitions = require("../JargonDefinitions.json");
    if (customKey) {
      // Use the key obtained from the previous parse to obtain the relevant value (phrase definition)
      console.log("Phrase selected:", customKey);
      // Populate the English text area with the phrase selected and its English definition
      setInputTextAreaValue(customKey + "\n\n" + jargonDefinitions["en"][0][customKey]);
      // Populate the non-English text area with the phrase selected and its non-English definition
      setOutputTextAreaValue(jargonDefinitions[langaugeSelected][0][customKey] + "\n\n" + jargonDefinitions[langaugeSelected][1][customKey]);
      // Return the format to English input (left side) and non-English output (right side)
      setIsEnglishInput(true);
    }
  }, [props.location.search, langaugeSelected]);

  // Function to translate text when translate button is pressed
  function translate() {
    setIsLoading(true);
    clearURLQuery();
    console.log("Translating: " + inputTextAreaValue);
    // Check if the switch language button has been pressed and change output language to English if so
    if (!isEnglishInput) {
      langaugeSelected = "en";
    }
    // Data to be POSTed
    const dataToPost = {
      text: inputTextAreaValue,
      to: langaugeSelected,
    };
    // Headers to be POSTed with data
    const headers = {
      accept: "application/json",
      "Content-Type": "application/json",
    };
    // POST request
    axios
      .post("https://peters-server.azurewebsites.net/translate", dataToPost, {
        headers,
      })
      .then((response) => {
        const translatedText = response.data[0].translations[0].text;
        console.log("response data", response.data[0].translations[0].text);
        setOutputTextAreaValue(translatedText);
        setIsLoading(false);
      });
  }

  // Function to dynamically update the inputTextAreaValue with each key press within the input text area
  function handleChange(event) {
    setInputTextAreaValue(event.target.value);
  }

  // Function to switch the input and output langauges
  function switchLangauges() {
    setIsEnglishInput(!isEnglishInput);
    clear();
    clearURLQuery();
  }

  // Function to clear all the text areas
  function clear() {
    setInputTextAreaValue("");
    setOutputTextAreaValue("");
    clearURLQuery();
  }
  
  // Variable used to refresh the URL to eliminate any previous query used
  const history = useHistory();
  function clearURLQuery() {
    // Clear any query from the URL (if there is one)
    history.push("/translate");
  }

  return (
    <div>
      <Helmet>
        <title>Language Translation | Translate</title>
      </Helmet>

      <IdleTimeout />

      {isLoading ? (
        <div className="loading">
          <Spinner color="dark" />
        </div>
      ) : (
          <div />
        )}
      <div className="centerButtons">
        <img
          className="imageBtn"
          src={SwitchIcon}
          title="Swap languages"
          alt="Swap languages"
          onClick={switchLangauges}
        />
      </div>
      <div className="translateTextBoxesContainer">
        {isEnglishInput ? (
          <span className="translateTextBoxLeft"><i>{engTextAreaTitle}</i> / <b>English</b></span>
        ) : (
            <span className="translateTextBoxLeft"><i>{nonEngTextAreaTitlePt1}</i> / <b>{nonEngTextAreaTitlePt2}</b></span>
          )}
        {isEnglishInput ? (
          <span className="translateTextBoxRight"><i>{nonEngTextAreaTitlePt1}</i> / <b>{nonEngTextAreaTitlePt2}</b></span>
        ) : (
            <span className="translateTextBoxRight"><i>{engTextAreaTitle}</i> / <b>English</b></span>
          )}
      </div>
      <div className="translateTextBoxesContainer">
        <div className="translateTextBoxLeft">
          <textarea
            type="text"
            name="EnglishInput"
            rows="12"
            placeholder={textAreaInputPlaceholder}
            value={inputTextAreaValue}
            onChange={handleChange}
          ></textarea>
          <div className="iconGridLayout">
            <div>
              <img
                className="imageBtn-inactive"
                src={MicrophoneIcon}
                title="Speech input currently unavailable"
                alt="Speech input currently unavailable"
              />
            </div>
            <div>
              <img
                className="imageBtn-inactive"
                src={VolumeIcon}
                title="Speech playback currently unavailable"
                alt="Speech playback currently unavailable"
              />
            </div>
            <div>
              <img
                className="imageBtn"
                src={ClearIcon}
                title="Clear"
                alt="Clear text"
                onClick={clear}
              />
            </div>
          </div>
        </div>
        <div className="translateTextBoxRight">
          <textarea
            type="text"
            name="VietnameseOutput"
            rows="12"
            placeholder={textAreaOutputPlaceholder}
            value={outputTextAreaValue}
            readOnly={true}
          ></textarea>
          <div className="iconGridLayout">
            <div>
              <img
                className="hideIcon"
                src={MicrophoneIcon}
              />
            </div>
            <div>
              <img
                className="imageBtn-inactive"
                src={VolumeIcon}
                title="Speech playback currently unavailble"
                alt="Speech playback"
              />
            </div>
            <div>
              <img
                className="hideIcon"
                src={ClearIcon}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="translateBtnContainer">
        <button
          className="translateBtn btnSpacing"
          title="Translate input text"
          onClick={translate}
          disabled={inputTextAreaValue.length < 1}
        >
          <i>{translateButtonText}</i> / <b>Translate</b>
        </button>
      </div>

      <CommonPhrases />
    </div>
  );
}

export default Translate;