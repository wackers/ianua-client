import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";

function LandingPage() {
  return (
    <div>
      <Helmet>
        <title>Ianua Language Translation</title>
      </Helmet>

      <div>
        <h1>STAFF USE ONLY</h1>
        <h2>Select office mode</h2>
      </div>
      <div className='buttonGrid2items'>
        <Link to={{ pathname: '/selectLanguage' }}>
          <button className='gridItems languageBtn-active' onClick={() => localStorage.setItem("officeMode", "frontOffice")}><b>Front of Office</b><br />(COVID-19 screening)</button>
        </Link>
        <Link to={{ pathname: '/selectLanguage' }}>
          <button className='gridItems languageBtn-active' onClick={() => localStorage.setItem("officeMode", "inOffice")}><b>In Office</b><br />(Translation service)</button>
        </Link>
      </div>
    </div>
  );
}

export default LandingPage;