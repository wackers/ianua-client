import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import IdleTimeout from "../IdleTimeout";
import { Helmet } from "react-helmet";

function CovidQuestion1() {
  // Retrieve the language selected at the home page from localStorage
  const [langaugeSelected] = useState(localStorage.getItem('languageSelected'));
  // Empty variables for all pre-filled areas of the COVID-19 Question 1 page that are changed according to the non-English language selected
  const [pageInstructions, setPageInstructions] = useState("");
  const [covidQuestion1, setCovidQuestion1] = useState("");
  const [yesText, setYesText] = useState("");
  const [noText, setNoText] = useState("");

  // Function that runs when the page is loaded
  useEffect(() => {
    // Switch statement to assign the pre-filled text with the relevant language chosen at the beginning
    switch (langaugeSelected) {
      // Vietnamese variables 
      case "vi":
        {
          setPageInstructions("Hãy trả lời những câu hỏi sau đây");
          setCovidQuestion1("Bạn đã được xác nhận với COVID-19 trong vòng 14 ngày qua chưa?");
          setYesText("Đúng");
          setNoText("Không");
          break;
        }
      // German variables
      case "de":
        {
          setPageInstructions("Bitte beantworten Sie die folgenden Fragen");
          setCovidQuestion1("Wurden Sie in den letzten 14 Tagen mit COVID-19 bestätigt?");
          setYesText("Ja");
          setNoText("Nein");
          break;
        }
      default:
        console.log("Error: No language selected");
    }
  }, [langaugeSelected]);

  return (
    <div className="body">
      <Helmet>
        <title>Language Translation | COVID Q1</title>
      </Helmet>

      <IdleTimeout />

      <h1>COVID-19</h1>
      <h2><i>{pageInstructions}</i><br /><b>Please answer the following questions</b></h2>
      <div className='covidQuestion-nonEng'><i>{covidQuestion1}</i></div>
      <div className='covidQuestion-eng'><b>Have you been confirmed with COVID-19 within the last 14 days?</b></div>
      <div className='yesNoBtnsContainer'>
        <Link to='/covid19-accessDenied'>
          <button className='yesBtn'><i>{yesText}</i><br /><b>Yes</b></button>
        </Link>
        <Link to={{ pathname: '/covid19-q2' }}>
          <button className='noBtn'><i>{noText}</i><br /><b>No</b></button>
        </Link>
      </div>
    </div>
  );
}

export default CovidQuestion1;